import express from 'express';
const app = express();

app.use(express.json());

app.post('/', (request, response) => {
  const name = request.body.name;
  console.log(request.body);
  response.json({ status: 'OK' });
});

//Start the server and make it listen for connections on 8888 by default
const PORT = process.env.PORT || 8888;
app.listen(PORT);

console.log(`listening on ${PORT}`);
