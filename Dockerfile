FROM node:current-alpine

# Default environment variables, modify to change
ENV PORT=8888

WORKDIR /usr/src/app

# Install app dependecies into WORKDIR
COPY package.json yarn.lock ./
RUN node -v && yarn -v
RUN yarn install

# Bundle app source in WORKDIR

COPY . .

EXPOSE $PORT

CMD [ "yarn", "start" ]
